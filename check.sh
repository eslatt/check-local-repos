check_branch () {
  branch_name=$1
  local_branch_head_hash=$(git rev-parse $branch_name 2>/dev/null)
  origin_branch_head_hash=$(git rev-parse origin/$branch_name 2>/dev/null)

 # check 4 - that the latest local commit has been pushed up
  if [ "$local_branch_head_hash" != "$origin_branch_head_hash" ]; then
    echo "    BAD 4 - non-current branch not up-to-date" $branch_name
    return 1
  else
    echo "    GOOD 4 -" $branch_name
    return 0
  fi
}

check_directory () {
  directory_name=$1
 # Push-Location $directory_name

 # get current branch
  current_branch=`git branch | grep \* | cut -d ' ' -f2`
  default_branch=`git remote show origin | sed -n '/HEAD branch/s/.*: //p'`
  echo "  " $directory_name "(" $current_branch ") default branch (" $default_branch ")"

 # check 1 - that there are no changes that need to be committed
  git status | grep Changes > /dev/null
  if [ "$?" == "0" ]; then
    echo "    BAD 1 - NOT up-to-date found change[s]"
 #   Pop-Location
    return
  fi
  echo "    GOOD 1 - current branch (" $current_branch ") is up-to-date"

 # check 2 - that there are no untracked files that need to be committed
  git status | grep Untracked > /dev/null
  if [ "$?" == "0" ]; then
    echo "    BAD 2 - NOT up-to-date found untracked[s]"
 #   Pop-Location
    return
  fi
  echo "    GOOD 2 - there are no untracked[s]"

 # check 3 - that the branch is up to date with the origin
  git status | grep "Your branch is up to date with 'origin/" > /dev/null
  if [ "$?" == "1" ]; then
    echo "    BAD 3 - NOT up-to-date not represented at origin"
 #   Pop-Location
    return
  fi
  echo "    GOOD 3 - current branch (" $current_branch ") is represented at origin"

 # checks 4 - that all branches are either pushed to origin or a scratch branch less than one year old
  branches=$(git branch --list | cut -c 3-)
  for branch in $branches; do
    if [[ $branch == scratch* ]]; then
 #       if($branch -match "^scratch/16" -or $branch -match "^scratch/17") # not clear I have to keep checking for previous years
 #       {
 #         Write-Host "cd ""$directory_name""; git branch -D $branch; cd .."
 #       }
 #       else
 #       {
 #        # Write-Host "    scratch branch " $branch
 #       }
      echo TODO port scratch branch work
      return
    else
      check_branch $branch
      if [ $? -ne 0 ]; then
        return
      fi
    fi
  done

 # check 5 - that all branches are either pulled back into develop or a scratch branch
  branches=$(git branch --no-merged $default_branch | cut -c 3-)
  for branch in $branches; do
    if [[ $branch == scratch* || $branch != $current_branch ]]; then
      echo "    BAD 5 - dangling" $branch
      return
    fi
  done
  echo "    GOOD 5 - no dangling brnaches"

  echo "    GOOD Clean"

#
 # Pop-Location
}

main () {
  echo
  echo '  ensuring all git repositories are fully represented at origin'
  echo
  check_directory .
 # ls . | Foreach-Object {
 #   if($_.PSIsContainer) {
 #     check_directory $_.Name
 #   }
 # }
# }
}



 # test_case () {
 #   skip_flag=$2
 #   if [ "$skip_flag" == "skip" ]; then
 #     echo skipping test $1
 #     cp ./control/$1.html ./audit
 #     cp ./control/$1-4inspect.json ./audit
 #     cp ./control/$1-metrics.yml ./audit
 #     cp ./control/$1-delivery-kit.json ./audit
 #     cp ./control/$1-dk.html ./audit
 #     cp ./control/$1-mavenlink-creates.js ./audit
 #     if [ -f ./control/$1-sow-activities.yml ]; then cp ./control/$1-sow-activities.yml ./audit; fi
 #     if [ -f ./control/$1-sow-description.json ]; then cp ./control/$1-sow-description.json ./audit; fi
 #   else
 #     cp ./test-resources/$1.json ./audit
 #     node gen-help.js ./audit/$1.json
 #     rm ./audit/$1.json
 #   fi
 # }

main