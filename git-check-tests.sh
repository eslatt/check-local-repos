

mkdir check-1
cd check-1
git init
echo check-1 > test.txt
git add --all
git commit -m first
echo second line >> test.txt
/Users/edslatt/e/edstash/2022-01-git-check/check.sh
cd ..

mkdir check-2
cd check-2
git init
echo check > test.txt
git add --all
git commit -m first
echo check > test2.txt
/Users/edslatt/e/edstash/2022-01-git-check/check.sh
cd ..

git clone git@gitlab.com:Slatt/git-check-test-3.git
mv git-check-test-3 check-3
cd check-3
echo check >> test.txt
git add --all
git commit -m second
/Users/edslatt/e/edstash/2022-01-git-check/check.sh
cd ..

git clone git@gitlab.com:Slatt/git-check-test-3.git
mv git-check-test-3 check-4
cd check-4
git checkout check-4
echo check >> test.txt
git add --all
git commit -m 'second commit in check-4'
git checkout main
/Users/edslatt/e/edstash/2022-01-git-check/check.sh
cd ..

git clone git@gitlab.com:Slatt/git-check-test-3.git
mv git-check-test-3 check-5
cd check-5
git checkout check-4
git checkout main
/Users/edslatt/e/edstash/2022-01-git-check/check.sh
cd ..

git clone git@gitlab.com:Slatt/git-check-test-3.git
mv git-check-test-3 check-clean
cd check-clean
/Users/edslatt/e/edstash/2022-01-git-check/check.sh
cd ..


rm -rf check-*
