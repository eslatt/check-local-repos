function check_branch($branch_name)
{
  $local_branch_head_hash = git rev-parse $branch_name 2>$null
  $origin_branch_head_hash = git rev-parse origin/$branch_name 2>$null
  if($local_branch_head_hash -notmatch $origin_branch_head_hash)
  {
    Write-Host "    NOT up-to-date" $branch_name
    Write-Host "     git checkout " $branch_name
    return
  }
}

function check_directory($directory_name)
{
  Push-Location $directory_name

 # get current branch
  $current_branch = git branch | grep \* | cut -d ' ' -f2
  Write-Host "  " $directory_name "(" $current_branch ")"

 # check that there are no changes that need to be committed
  git status | findstr /C:"Changes" 2>&1 | out-null
  if(!$LastExitCode)
  {
    Write-Host "    NOT up-to-date found change[s]"
    Pop-Location
    return
  }

 # check that there are no untracked files that need to be committed
  git status | findstr /C:"Untracked" 2>&1 | out-null
  if(!$LastExitCode)
  {
    Write-Host "    NOT up-to-date found untracked[s]"
    Pop-Location
    return
  }

 # check that the branch is up to date with the origin
  git status | findstr /C:"Your branch is up to date with 'origin/" 2>&1 | out-null
  if($LastExitCode)
  {
    Write-Host "    NOT up-to-date not represented at origin"
    Pop-Location
    return
  }

 # checks that all branches are either pushed to origin or a scratch branch less than one year old
  $git_branch_output = git branch
  ForEach ($line in $($git_branch_output -split "`r`n"))
  {
      $branch = $line.Substring(2,$line.length-2)
      if($branch -notmatch "^scratch")
      {
          check_branch $branch
      }
      else
      {
        if($branch -match "^scratch/16" -or $branch -match "^scratch/17") # not clear I have to keep checking for previous years
        {
          Write-Host "cd ""$directory_name""; git branch -D $branch; cd .."
        }
        else
        {
         # Write-Host "    scratch branch " $branch
        }
      }
  }

 # checks that all branches are either pulled back into develop or a scratch branch
  $git_branch_output = git branch --no-merged develop
  ForEach ($line in $($git_branch_output -split "`r`n"))
  {
      $branch = $line.Substring(2,$line.length-2)
      if($branch -notmatch "^scratch" -and $branch -notmatch $current_branch)
      {
          Write-Host "    dangling" $branch
      }
  }

  Pop-Location
}

function main
{
  Write-Host ""
  Write-Host "  ensuring all git repositories are fully represented at origin"
  Write-Host ""
  ls . | Foreach-Object {
    if($_.PSIsContainer) {
      check_directory $_.Name
    }
  }
}

main